import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is for Active Batch capacity planning.
 */

/**
 * @author asahu
 *
 */
public class ScheduleReporting {
	
	
	static InputStream inputStream;	
	static Connection connctn ;

		public static void main(String[] args){
		
//			String path = ScheduleReporting.class.getClassLoader().getResource("").getPath();
			String path = ScheduleReporting.class.getResource("").getPath();	
			path = path.replace("bin/", "") ;			
			System.out.println(path);
			
			String pt = path+"schedulesFetch.vbs" ;
			
			pt = pt.substring(1);
			System.out.println(pt);
			
			
			String jssName = args[0] ; 
			String fpath = args[1] ; 
			String allSchedules = "" ;
			String outputMessage = "" ;
			
			 int exitStatus = 98;
			
			 try {
				 				 
				 ArrayList<String> al = new ArrayList<String>() ;
				 
				 
				 System.out.println("Executing script for retrieving all available Schedules");
				 String command = "cscript "+ pt +" \"" + jssName +"\" " +"\"" + fpath +"\" ";
				 System.out.println("Executing Command : " + command);
				 Process p = Runtime.getRuntime().exec(command);
//				 
				 String tmp1 ;
				 BufferedReader stdinput = new BufferedReader(new InputStreamReader(p.getInputStream()));
					
					while ((tmp1 = stdinput.readLine()) != null) {
						
						//System.out.println(tmp1);
						allSchedules = allSchedules.concat(tmp1) ;
						
						
						
						Pattern pattern = Pattern.compile("All rights reserved.(.*?) Script Execution Successful");
						Matcher matcher = pattern.matcher(allSchedules);
						while (matcher.find()) {
						    //System.out.println(matcher.group(1));
						    allSchedules = matcher.group(1) ;
						    
						  String[] ss =  allSchedules.split("Next Reference") ; 
						  
						  
						  
						  for(String s: ss) {
							  
							  if(!s.equals("") && s!= null) 
							  {
								  al.add(s) ;
							}							
						  }
						  
						  
						  for(int i=0; i<al.size();i++) {
							  
							  al.set(i, al.get(i).trim()) ;
							  al.set(i, al.get(i).substring(0, al.get(i).length()-1)) ;							  
							  System.out.println(i+"--"+al.get(i));
															  
						  }
						  						  
						}
						
					}
					boolean b = true ; 
					
					b = doTruncate(fpath) ;
					
					if(b==false) {
						
						System.out.println("Error occured while Truncate and load, Throwing exception");
						throw new SQLException() ;
						
					}
					
					Connection conn = getConnection() ;
					
				for(String aa : al) {
					
					String[] sar = aa.split(",") ;
					
					for(int i =1;i<sar.length;i++) {
												
						
						
						b = doInsert(sar[0],sar[i],fpath,conn) ; 

						
						System.out.println(b);
						if(b==false) {
						
							System.out.println("Error occured while Inserting records, Throwing exception");
							throw new SQLException() ;
							
						}						
					}
					
				}	
								    					
//				 
				 p.waitFor();
				 exitStatus = p.exitValue();
				 System.out.println(exitStatus);
				 
				 if(exitStatus==98) {					 
					 
					 System.out.println("VB script execution Succeeded, Executing success status block");				 
					// System.out.println("All available schedules are : "+ allSchedules); 					    
					 outputMessage = "VBscript executed successfully, All schedules are fetched" ;
					 System.out.println(outputMessage);
					 
					 
				 }
				 else {				 
					
					allSchedules=""; 				 
					String tmp ;
										 
					BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					System.out.println("VB script execution failed, Executing fail status block");
					
					while ((tmp = stdError.readLine()) != null) {
										
						outputMessage = outputMessage.concat(tmp) ;	
						
					}				 	
					
					outputMessage = "VB script execution failed, Error is : " + outputMessage ;
					System.out.println(outputMessage);
				}
				
			   
			 }
			   catch(Exception e ) {
			      System.out.println(e);
			      e.printStackTrace();
			   }	
		 
		 }
		
		
		
		static Connection getConnection() {
			
			try {
			Properties prop = new Properties();
			String propFileName = "db.properties";
 
			inputStream = ScheduleReporting.class.getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

 
			String sourceTableDriver = prop.getProperty("sourceTableDriver");
			String sourceTableUrl = prop.getProperty("sourceTableUrl");
			String sourceTableUsername = prop.getProperty("sourceTableUsername");
			String sourceTablePassword = prop.getProperty("sourceTablePassword");
			
			 connctn = DBUtility.getConnection(sourceTableDriver, sourceTableUrl, sourceTableUsername,
					sourceTablePassword);
			 
			 
			}
			
			catch(Exception e) {
				e.printStackTrace();
				System.out.println("Error while getting the connection object for insert");
				System.exit(9) ;
			}
			return connctn ;
		}
		
		
		
		static boolean  doInsert(String refName, String scheduleName, String fpath, Connection connctn) {
			
			try {
							
				 
				 PreparedStatement statement = connctn.prepareStatement("insert into Atlas.dataplatform.ReferenceSchedules values(?,?,?) "); 
					
				   	statement.setString(1, refName);    
					statement.setString(2, scheduleName);
					statement.setString(3, fpath);
					
					System.out.println(statement);
					
					try {
												
						statement.execute();
					}catch(Exception sice) {						
						
						sice.printStackTrace();
						System.out.println("aaaaaa");
						return false ;				
						
					}			 
				
				
			} catch (Exception e) {
								
				e.printStackTrace();
				System.out.println("bbbbb");
				return false ;
			} 
			finally {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
								
			return true ;
		}
				
		
        static boolean  doTruncate(String folderName) {
			
			try {
				Properties prop = new Properties();
				String propFileName = "db.properties";
				
				System.out.println("Deleting the records for this folder : "+folderName);
	 
				inputStream =  ScheduleReporting.class.getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}

	 
				String sourceTableDriver = prop.getProperty("sourceTableDriver");
				String sourceTableUrl = prop.getProperty("sourceTableUrl");
				String sourceTableUsername = prop.getProperty("sourceTableUsername");
				String sourceTablePassword = prop.getProperty("sourceTablePassword");
				
				 connctn = DBUtility.getConnection(sourceTableDriver, sourceTableUrl, sourceTableUsername,
						sourceTablePassword);
				
				 
				
					
				 Statement stmt = connctn.createStatement() ;
										
					try {
						
						int i = stmt.executeUpdate("Delete from Atlas.dataplatform.ReferenceSchedules where FolderName = '"+folderName+"'") ;
						System.out.println("Total no of records deleted = "+i);
						
					}catch(Exception sice) {						
						
						sice.printStackTrace();
						System.out.println("aaaaaa");
						return false ;				
						
					}			 
				
				
			} catch (Exception e) {
								
				e.printStackTrace();
				System.out.println("bbbbb");
				return false ;
			} 
			finally {
				try {
					inputStream.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}	
								
			return true ;
		}
		
		
		
		
	}
